﻿using System;
using ServiceStack;
using ServiceStack.Text;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using McMaster.Extensions.CommandLineUtils;
using System.Globalization;
using System.Text;

namespace ElmAnalyseToJUnit
{
    public class ElmAnalyseOutput
    {
        public List<MessageData> Messages { get; set; }
        public ModulesData Modules { get; set; }
        public List<String> UnusedDependencies { get; set; }
        public class ModulesData
        {
            public List<List<string>> ProjectModules { get; set; }
            public List<List<List<string>>> Dependencies { get; set; }

        }
        public class MessageData
        {
            public enum TypeEnum { UnusedPatternVariable }
            public enum StatusEnum { Applicable }
            public class DataData
            {
                public class PropertiesData
                {
                    public List<int> Range { get; set; }
                    public string VarName { get; set; }
                }
                public string Description { get; set; }
                public PropertiesData Properties { get; set; }
            }
            public long Id { get; set; }
            public StatusEnum Status { get; set; }
            public string File { get; set; }
            public TypeEnum Type { get; set; }
            public DataData Data { get; set; }
        }
    }

    [XmlRoot("testsuite")]
    public class JUnitTestSuite
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "package")]
        public string Package { get; set; }
        [XmlAttribute(AttributeName = "tests")]
        public int Tests { get; set; }
        [XmlAttribute(AttributeName = "failures")]
        public int Failures { get; set; }
        [XmlAttribute(AttributeName = "errors")]
        public int Errors { get; set; }
        [XmlElement(ElementName = "testcase")]
        public List<TestcaseData> Testcase { get; set; }

        public class TestcaseData
        {
            [XmlAttribute(AttributeName = "classname")]
            public string Classname { get; set; }
            [XmlAttribute(AttributeName = "name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "failure")]
            public string Failure { get; set; }
        }
    }



    class Program
    {
        static int Main(string[] args)
        {

            var app = new CommandLineApplication();

            app.HelpOption();
            var inputFilepath = app.Option("-f|--inputFile <PATH>", "A file with `elm-analyse --format json` output.", CommandOptionType.SingleValue);
            var outputFilepath = app.Option("-o|--outputFile <PATH>", "The path of a filename for writing output.", CommandOptionType.SingleValue);

            app.OnExecute(() =>
            {
                JUnitTestSuite Convert(ElmAnalyseOutput ea)
                {
                    var result = new JUnitTestSuite
                    {
                        Name = "Elm Analyse",
                        Package = "Elm",
                        Tests = (ea.Modules?.ProjectModules?.Count ?? 0) + ea.Messages.Count,
                        Failures = ea.Messages.Count(),
                        Errors = 0,
                        Testcase = ea.Messages.ConvertAll(m => new JUnitTestSuite.TestcaseData
                        {
                            Name = m.File,
                            Failure = m.Data?.Description
                        })
                    };
                    return result;
                }
                string ToXml(object obj)
                {
                    var settings = new XmlWriterSettings
                    {
                        Indent = true,
                        // Since the Encoding doesn't affect the declaration, that always becomes
                        //  <?xml version="1.0" encoding="utf-16"?>
                        Encoding = Encoding.GetEncoding("UTF-8"),
                        // Avoid setting the XML Declaration altogether.
                        OmitXmlDeclaration = true
                    };

                    var namespaces = new XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);

                    var serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());

                    using (var stringWriter = new System.IO.StringWriter())
                    {
                        using (var xmlWriter = XmlWriter.Create(stringWriter, settings))
                        {
                            serializer.Serialize(xmlWriter, obj, namespaces);
                        }
                        return stringWriter.ToString();
                    }
                }


                var text = inputFilepath.HasValue() ? System.IO.File.ReadAllText(inputFilepath.Value())
                    : System.Console.IsInputRedirected ? System.Console.In.ReadToEnd()
                    : null;

                var data = text.FromJson<ElmAnalyseOutput>();

                var jUnit = Convert(data);
                var xml = ToXml(jUnit);

                if (outputFilepath.HasValue())
                    System.IO.File.WriteAllText(outputFilepath.Value(), xml);
                else if (System.Console.IsOutputRedirected)
                    System.Console.Out.Write(xml);
                else
                    System.Console.Out.WriteLine(xml);

                return 0;
            });

            return app.Execute(args);
        }
    }


}


