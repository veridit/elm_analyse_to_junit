# ElmAnalyseToJUnit

A command line tool that converts the json output from `elm-analyse --format json` to
JUnit xml for usage in a CI (Continuous Integration) pipeline.
